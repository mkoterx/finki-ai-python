from final import *

class Vojnik(Problem):

    def successor(self, state):
        succs = {}

        x, y = state
        next_state = (x, y + 2)
        if next_state not in hot_lava and next_state not in barriers:
            succs["move up"] = next_state

        next_state = (x, y - 2)
        if next_state not in hot_lava and next_state not in barriers:
            succs["move down"] = next_state

        next_state = (x + 1, y + 1)
        if next_state not in hot_lava and next_state not in barriers:
            succs["move up-right"] = next_state

        next_state = (x + 1, y - 1)
        if next_state not in hot_lava and next_state not in barriers:
            succs["move down-right"] = next_state

        next_state = (x - 1, y + 1)
        if next_state not in hot_lava and next_state not in barriers:
            succs["move up-left"] = next_state

        next_state = (x - 1, y - 1)
        if next_state not in hot_lava and next_state not in barriers:
            succs["move down-left"] = next_state

        return succs


    def actions(self, state):
        return self.successor(state).keys()

    def h(self, node):
        return euclidean_distance(node.state, self.goal)

    def result(self, state, action):
        possible = self.successor(state)
        return possible[action]

"""
    Presmetuva evklidovo rastojanie od edna do druga tocka, 
    so izmena na toa sto restojanieto od x1 do x2 e pomnozeno so 2 
    bidejki koordinatniot sistem e postaven taka sto vertikalnite 
    koordinatni tocki se dvojno pogusto postaveni od horizontalnite.
"""
def euclidean_distance(P1, P2):
    x1, y1 = P1
    x2, y2 = P2
    return math.sqrt(pow(2*(x2 - x1), 2) + pow(y2 - y1, 2))

# koordinati na polinja koi se nadvor od sistemot (ja predstavuvaat ramkata) i ne moze da se odi po niv
hot_lava = [(-6, 2), (-6, 0), (-6, -2), (-6, -4), (-5, -3), (-5, -1), (-5, -5), (-5, -7), (-4, 4), (-4, -8), (-4, 5),
         (-3, -9), (-2, 6), (-2, -8), (-1, 5), (-1, -7), (0, 4), (0, -8), (1, 5), (1, -9), (2, 6), (2, -8), (3, 5),
         (3, -7), (4, 4), (4, 2), (4, -2), (4, -6), (5, 1), (5, -1), (5, -3), (5, -5)]

# koordinati na preprekite
barriers = [(-2, 4), (2, 4), (-2, 0), (-3, -1), (-3, -5), (-1, -5), (1, -1), (1, -3), (-2, -2), (3, -5)]

# koordinati na zvezdite
stars = [(0, 2), (0, -2), (-4, -4), (4, 0)]

# pocetna tocka kade se naogja vojnikot
initial = (-4, 2)

# koordinatniot pocetok (0,0) e plockata pomegju dvete svezdi koi se vertikalno edna nad druga

result = []
while len(stars) > 0:
    min = 999999999999
    next_star = None
    # ja barame zvezdata (vozdusno) najbliska od momentalnata pozicija
    for star in stars:
        dist = euclidean_distance(initial, star)
        if dist < min:
            min = dist
            next_star = star
    problem = Vojnik(initial=initial, goal=next_star) # najbliskata zvezda ja postavuvame za goal
    answer = astar_search(problem)
    [result.append(point) for point in answer.solve()[1:]]
    initial = next_star # celnata zvezda sega stanuva pocetna sostojba
    stars.remove(next_star)

print("Broj na cekori: " + str(len(result)))
print(result)

"""
Sekoja sostojba e definirana so pozicijata na vojnikot vo odnos na koordinatniot pocetok koj e zemen da bide poleto 
koe se naogja pomegju dvete svezdi postaveni vertikalno edna pod druga

Funkcijata za tranzicija od edna vo druga sostojba proveruva dali akcijata koja predstavuva dvizenje vo odreden pravec
ke go odnese vojnikot vo pole koe e prepreka ili vrela lava. Dokolku ne, akcijata za pridvizuvanje vo soodvetniot pravec
kako i sostojbata do koja ke go donese istata se zapisuvaat vo recnikot.

Hevristikata pretstavuva evklidovo rastojanie od edno do drugo pole. Napravena e mala izmena vo formulata i toa e 
dokumentirano vo opisot na samata fukcija euclidean_distance.

Dokolku se promeni tablata ili brojot i rasporedot na prepreki i zvezdi, se sto treba da se napravi e 
da se odredi koe pole ke bide koordinatniot pocetok i spored nego da se zapisat koordinatite na zvezdite, 
preprekite i hot lava polinjata vo stars, barriers i hot_lava listite soodvetno.
"""

