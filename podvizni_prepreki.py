from final import *


def generate_view(num_rows, num_cols, man, home, obstacles, del_cells=None):
    matrix = [['.' for i in range(num_cols)] for i in range(num_rows)]

    if del_cells:
        for (row, col) in del_cells:
            matrix[row][col] = " "

    x, y = man
    matrix[x][y] = 'M'

    x, y = home
    matrix[x][y] = 'H'

    for o in obstacles:
        for (x, y) in o[1:]:
            matrix[x][y] = '@'

    # convert [[]] into (()) in order to be hashable
    res = []
    for row in matrix:
        res.append(tuple(row))
    return tuple(res)


def euclidean_distance(p1, p2):
    x1, y1 = p1
    x2, y2 = p2
    return math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)


class Istrazuvac(Problem):

    def goal_test(self, state):
        """Return True if the state is a goal. The default method compares the
        state to self.goal, as specified in the constructor. Implement this
        method if checking against a single self.goal is not enough."""
        man = state[0]
        return man == self.goal

    def successor(self, state):
        result = {}

        row, col = state[0]

        obstacles = state[1]

        obstacle_a = obstacles[0]
        o_row1, o_col1 = obstacle_a[1]
        o_row2, o_col2 = obstacle_a[2]
        direction = obstacle_a[0]
        if (o_col1 == 0 and direction == -1) or (o_col2 == 5 and direction == 1):
            direction = -direction
        obstacle_a = (direction, (o_row1, o_col1 + direction), (o_row2, o_col2 + direction))  # the obstacle is moving

        obstacle_b = obstacles[1]
        o_row1, o_col1 = obstacle_b[1]
        o_row2, o_col2 = obstacle_b[2]
        o_row3, o_col3 = obstacle_b[3]
        o_row4, o_col4 = obstacle_b[4]
        direction = obstacle_b[0]
        if (o_col2 == 5 and direction == 1) or (o_col3 == 0 and direction == -1):
            direction = -direction
        obstacle_b = (direction,
                      (o_row1 - direction, o_col1 + direction), (o_row2 - direction, o_col2 + direction),
                      (o_row3 - direction, o_col3 + direction), (o_row4 - direction, o_col4 + direction))

        obstacle_c = obstacles[2]
        o_row1, o_col1 = obstacle_c[1]
        o_row2, o_col2 = obstacle_c[2]
        direction = obstacle_c[0]
        if (o_row1 == 5 and direction == -1) or (o_row2 == 10 and direction == 1):
            direction = -direction
        obstacle_c = (direction, (o_row1 + direction, o_col1), (o_row2 + direction, o_col2))  # the obstacle is moving
        
        obstacles = tuple([obstacle_a, obstacle_b, obstacle_c])
        obstacles_cells = [cell for o in obstacles for cell in o[1:]]

        new_pos = (row - 1, col)
        if new_pos not in hot_lava and new_pos not in obstacles_cells:
            view = generate_view(NUM_ROWS, NUM_COLS, new_pos, GOAL, obstacles, del_cells=DEL_CELLS)
            next_state = (new_pos, obstacles, view)
            result["Up"] = next_state

        new_pos = (row + 1, col)
        if new_pos not in hot_lava and new_pos not in obstacles_cells:
            view = generate_view(NUM_ROWS, NUM_COLS, new_pos, GOAL, obstacles, del_cells=DEL_CELLS)
            next_state = (new_pos, obstacles, view)
            result["Down"] = next_state

        new_pos = (row, col + 1)
        if new_pos not in hot_lava and new_pos not in obstacles_cells:
            view = generate_view(NUM_ROWS, NUM_COLS, new_pos, GOAL, obstacles, del_cells=DEL_CELLS)
            next_state = (new_pos, obstacles, view)
            result["Right"] = next_state

        new_pos = (row, col - 1)
        if new_pos not in hot_lava and new_pos not in obstacles_cells:
            view = generate_view(NUM_ROWS, NUM_COLS, new_pos, GOAL, obstacles, del_cells=DEL_CELLS)
            next_state = (new_pos, obstacles, view)
            result["Left"] = next_state

        new_pos = (row - 1, col + 1)
        if new_pos not in hot_lava and new_pos not in obstacles_cells:
            view = generate_view(NUM_ROWS, NUM_COLS, new_pos, GOAL, obstacles, del_cells=DEL_CELLS)
            next_state = (new_pos, obstacles, view)
            result["Up-Right"] = next_state

        new_pos = (row - 1, col - 1)
        if new_pos not in hot_lava and new_pos not in obstacles_cells:
            view = generate_view(NUM_ROWS, NUM_COLS, new_pos, GOAL, obstacles, del_cells=DEL_CELLS)
            next_state = (new_pos, obstacles, view)
            result["Up-Left"] = next_state

        new_pos = (row + 1, col + 1)
        if new_pos not in hot_lava and new_pos not in obstacles_cells:
            view = generate_view(NUM_ROWS, NUM_COLS, new_pos, GOAL, obstacles, del_cells=DEL_CELLS)
            next_state = (new_pos, obstacles, view)
            result["Down-Right"] = next_state

        new_pos = (row + 1, col - 1)
        if new_pos not in hot_lava and new_pos not in obstacles_cells:
            view = generate_view(NUM_ROWS, NUM_COLS, new_pos, GOAL, obstacles, del_cells=DEL_CELLS)
            next_state = (new_pos, obstacles, view)
            result["Down-Left"] = next_state

        return result

    def actions(self, state):
        return self.successor(state).keys()

    def h(self, node):
        curr_pos = node.state[0]
        return euclidean_distance(curr_pos, self.goal)

    def result(self, state, action):
        possible = self.successor(state)
        return possible[action]


# polinja koi se nadvor od sistemot (ja predstavuvaat ramkata) i ne moze da se odi po niv
hot_lava = [(-1, -1), (0, -1), (1, -1), (2, -1), (3, -1), (4, -1), (5, -1), (6, -1), (7, -1), (8, -1), (9, -1),
            (10, -1), (11, -1), (11, 0), (11, 1), (11, 2), (11, 3), (11, 4), (11, 5), (11, 6), (11, 7), (11, 8),
            (11, 9), (11, 10), (11, 11), (-1, 0), (-1, 1), (-1, 2), (-1, 3), (-1, 4), (-1, 5), (-1, 6), (0, 6), (1, 6),
            (2, 6), (3, 6), (4, 6), (4, 7), (4, 8), (4, 9), (4, 10), (4, 11), (4, 11), (5, 11), (6, 11), (7, 11),
            (8, 11), (9, 11), (10, 11)]

"""
    Moving directions for different types of obstacles
    |OBSTACLE TYPE |        1|        -1|
    |------------------------------------
    |vertical      |     down|        up|
    |horizontal    |    right|      left|
    |diagonal      | up-right| down-left|  
"""

# Define the obstacles (first number gives the moving direction)
obstacle_h = (-1, (2, 2), (2, 3))                  # horizontal obstacle
obstacle_d = (+1, (7, 2), (7, 3), (8, 2), (8, 3))  # diagonal obstacle
obstacle_v = (+1, (7, 8), (8, 8))                  # vertical obstacle
obstacles = tuple([obstacle_h, obstacle_d, obstacle_v])

# Read input
man_row = int(input())
man_col = int(input())
home_row = int(input())
home_col = int(input())

# Define the table
NUM_ROWS = 11
NUM_COLS = 11
DEL_CELLS = []  # contains row and column number of the cells that need to be deleted from the view
for row in range(0, 5):
    for col in range(6, 11):
        DEL_CELLS.append((row, col))

view = generate_view(NUM_ROWS, NUM_COLS, (man_row, man_col), (home_row, home_col), obstacles, del_cells=DEL_CELLS)

initial = ((man_row, man_col), obstacles, view)  # Initial state
GOAL = (home_row, home_col)

problem = Istrazuvac(initial=initial, goal=GOAL)
solution_node = astar_search(problem)

print(solution_node.solution())
solution_node.visualize_solution()  # for each state prints the view associated with that state