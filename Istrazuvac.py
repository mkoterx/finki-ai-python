from final import *
import numpy


class Istrazuvac(Problem):

    def goal_test(self, state):
        """Return True if the state is a goal. The default method compares the
        state to self.goal, as specified in the constructor. Implement this
        method if checking against a single self.goal is not enough."""
        covece = state[0]
        return covece == self.goal

    def successor(self, state):
        succs = {}

        row, col = state[0]

        obstacle_a = state[1]
        o_row, o_col = obstacle_a[0]
        direction = obstacle_a[1]
        if (o_row == 1 and direction == -1) or (o_row == 6 and direction == 1):
            direction = -direction
        obstacle_a = ((o_row + direction, o_col), direction)  # se pomestuva preckata

        obstacle_b = state[2]
        o_row, o_col = obstacle_b[0]
        direction = obstacle_b[1]
        if (o_row == 1 and direction == -1) or (o_row == 6 and direction == 1):
            direction = -direction
        obstacle_b = ((o_row + direction, o_col), direction)  # se pomestuva preckata
        obstacles = [obstacle_a, obstacle_b]

        new_pos = (row - 1, col)
        next_state = (new_pos, obstacle_a, obstacle_a)
        if new_pos not in hot_lava and new_pos not in [(o[0]) for o in obstacles]:
            succs["Up"] = next_state

        new_pos = (row + 1, col)
        next_state = (new_pos, obstacle_b, obstacle_a)
        if new_pos not in hot_lava and new_pos not in [(o[0]) for o in obstacles]:
            succs["Down"] = next_state

        new_pos = (row, col + 1)
        next_state = (new_pos, obstacle_a, obstacle_b)
        if new_pos not in hot_lava and new_pos not in [(o[0]) for o in obstacles]:
            succs["Right"] = next_state

        new_pos = (row, col - 1)
        next_state = (new_pos, obstacle_a, obstacle_b)
        if new_pos not in hot_lava and new_pos not in [(o[0]) for o in obstacles]:
            succs["Left"] = next_state

        return succs

    def actions(self, state):
        return self.successor(state).keys()

    def h(self, node):
        curr_pos = node.state[0]
        return euclidean_distance(curr_pos, self.goal)

    def result(self, state, action):
        possible = self.successor(state)
        return possible[action]

def euclidean_distance(P1, P2):
    x1, y1 = P1
    x2, y2 = P2
    return math.sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2))


# koordinati na polinja koi se nadvor od sistemot (ja predstavuvaat ramkata) i ne moze da se odi po niv
hot_lava = [(0, 0), (1, 0), (2, 0), (3, 0), (4, 0), (5, 0), (6, 0), (7, 0), (7, 1), (7, 2), (7, 3),
            (7, 4), (7, 5), (7, 6), (7, 7), (7, 8), (7, 9), (0, 1), (0, 2), (0, 3), (0, 4), (0, 5), (0, 6),
            (0, 7), (0, 8), (0, 9), (1, 9), (2, 9), (3, 9), (4, 9), (5, 9), (6, 9), (7, 9)]

# matrix = [['*' for i in range(8)] for i in range(6)]
#
# matrix[0][2]='P'
# matrix[5][5]="P"
# matrix[1][7]="H"
# matrix[3][0]="M"
# for row in matrix:
#     for el in row:
#         print('{:>3}'.format(el), end="")
#     print()

# -1 for moving upwards, 1 for moving downwards
obstacleA = ((6, 3), -1)
obstacleB = ((2, 6), -1)

CoveceRedica = int(input())
CoveceKolona = int(input())
KukaRedica = int(input())
KukaKolona = int(input())

initial = ((CoveceRedica, CoveceKolona), obstacleA, obstacleB)
goal = (KukaRedica, KukaKolona)

problem = Istrazuvac(initial=initial, goal=goal)
answer = astar_search(problem)
print(answer.solution())
# for (x, y, z) in answer.solve():
#     print(str(x) + " : " + str(y))