from final import *
import re


def generate_view(num_rows, num_cols, tanks, missiles):
    matrix = [['.' for i in range(num_cols)] for i in range(num_rows)]

    for m in missiles:
        x, y = m.position
        matrix[x][y] = '*'

    for t in tanks:
        x, y = t.position
        matrix[x][y] = str(t.shield)

    # convert [[]] into (()) in order to be hashable
    res = []
    for row in matrix:
        res.append(tuple(row))
    return tuple(res)


class TankDestroyer(Problem):

    def goal_test(self, state):
        """Return True if the state is a goal. The default method compares the
        state to self.goal, as specified in the constructor. Implement this
        method if checking against a single self.goal is not enough."""
        tanks_left = len(state[0])
        return not tanks_left

    def successor(self, state):
        result = {}

        tanks = list(state[0])
        missiles = list(state[1])
        num_missiles = len(missiles)

        if num_missiles:
            for i in range(0, num_missiles):
                missiles[i].move()

            missiles = [m for m in missiles if is_inside_table(m.position, NUM_ROWS, NUM_ROWS)]


            for m in missiles:
                for tank in tanks:
                    if m.position == tank.position:
                        missiles.remove(m)
                        tank.shield = tank.shield - 1
                        if tank.shield == -1:
                            new_missiles = tank.fire_missiles()
                            missiles = missiles + new_missiles
                            tanks.remove(tank)
            print(missiles)
            view = generate_view(NUM_ROWS, NUM_ROWS, tuple(tanks), tuple(missiles))
            next_state = (tuple(tanks), tuple(missiles), view)
            result["None"] = next_state
        else:  # player selects action
            for tank in tanks:
                copy = tanks[:]
                copy.remove(tank)
                t = Tank(tank.position, tank.shield)
                t.shield = t.shield - 1
                if t.shield == -1:
                    missiles = t.fire_missiles()
                else:
                    missiles = ()
                    copy.append(t)
                view = generate_view(NUM_ROWS, NUM_ROWS, tuple(copy), tuple(missiles))
                next_state = (tuple(copy), tuple(missiles), view)
                result["Hit tank at pos " + str(tank.position)] = next_state

        return result

    def actions(self, state):
        return self.successor(state).keys()

    def h(self, node):
        return 0

    def result(self, state, action):
        possible = self.successor(state)
        return possible[action]

    def path_cost(self, c, state1, action, state2):
        return c + 1


def is_inside_table(pos, num_rows, num_cols):
    row, col = pos
    return 0 <= row < num_rows - 1 and 0 <= col < num_cols - 1


class Tank:

    def __init__(self, position, shield):
        self.position = position
        self.shield = shield

    def fire_missiles(self):
        result = []
        directions = ["u", "r", "d", "l"]
        for d in directions:
            result.append(Missile(self.position, direction=d))
        return result

    def __repr__(self):
        return "Tank(" + str(self.shield) + ")"

    def __eq__(self, other):
        """Override the default Equals behavior"""
        if isinstance(other, self.__class__):
            return self.position == other.position
        return False

class Missile:

    def __init__(self, position, direction):
        self.position = position
        if re.match('[urdl]', direction):
            self.direction = direction
        else:
            raise NotImplementedError

    def move(self):
        row, col = self.position
        if self.direction == "u":
            self.position = (row - 1, col)
        elif self.direction == "r":
            self.position = (row, col + 1)
        elif self.direction == "d":
            self.position = (row + 1, col)
        else:
            self.position = (row, col - 1)

    def __repr__(self):
        return "Missile" + str(self.position)

    def __eq__(self, other):
        """Override the default Equals behavior"""
        if isinstance(other, self.__class__):
            return self.position == other.position and self.direction == other.direction
        return False

tanks = []

# Read input
NUM_ROWS = int(input())
num_tanks = int(input())
for i in range(0, num_tanks):
    row = int(input())
    col = int(input())
    shield = int(input())
    pos = (row, col)
    t = Tank(pos, shield)
    tanks.append(t)

missiles = []

view = generate_view(NUM_ROWS, NUM_ROWS, tuple(tanks), tuple(missiles))
initial = (tuple(tanks), tuple(missiles), view)

problem = TankDestroyer(initial=initial)
solution_node = breadth_first_tree_search(problem)
print(solution_node.solution())
solution_node.visualize_solution()





